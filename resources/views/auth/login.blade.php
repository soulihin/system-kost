@extends('layouts.app')

@section('title')
Login
@endsection

@section('content')
<style>
  @media (max-width: 600px) {
    .background-walk-y {
      display: none !important;
    }
  }
</style>

<div class="d-flex flex-wrap align-items-stretch">
  <div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
    <div class="p-4 m-3">
      <center>
        <h1 class="mb-2"> <span class="text-success">U.</span>K</h1>
        <h4 class="text-dark font-weight-normal">Selamat Datang di <span class="font-weight-bold"><span class="text-success">Usakti</span> Kos</span></h4>
      </center>
      <form method="POST" action="{{route('login')}}" class="needs-validation" novalidate="">
        @csrf
        <div class="form-group">
          <label for="email">Email</label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          @error('email')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>

        <div class="form-group">
          <div class="d-block">
            <label for="password" class="control-label">Password</label>
          </div>
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

          @error('password')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>



        <div class="form-group text-right">
          <!-- <a href="auth-forgot-password.html" class="float-left mt-3">
                  Forgot Password?
                </a> -->

          <div class="mt-3 text-center">
            Don't have an account? <a href="{{route('register')}}">Create new one</a>
          </div><br>
          <center>
            <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" tabindex="4">
              Login
            </button>
          </center>
        </div>

      </form>
    </div>
  </div>
  <div class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom" data-background="{{ asset('backend/assets/img/login-bg.jpg') }}">
    <div class="absolute-bottom-left index-2">
    </div>
  </div>
</div>
@endsection