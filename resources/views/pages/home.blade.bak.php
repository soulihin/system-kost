@extends('layouts.landingPage')

@section('title')
Home
@endsection

@section('content')
<style>
    .text-brand {
        color: #fff !important;
    }

    .nav-link {
        color: #fff !important;
    }
</style>
<!--/ Carousel Star /-->
<!-- <div class="section-t8">
    <div class="container">
        <div class="mt-4 row">
            <div class="col-md-6">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('frontend/img/slide-1.jpg') }}" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('frontend/img/slide-2.jpg') }}" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('frontend/img/slide-3.jpg') }}" alt="Third slide">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h3>Kosan <span class="color-b">Trisakti</span> </h3>
                <h6>Temukan Kosan Idamanmu diwebsite ini</h6>
            </div>
        </div>
    </div>
</div> -->
<!--/ Carousel end /-->

<!--/ Services Star /-->
<section class="section-services section-t8">
    <!-- <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-wrap d-flex justify-content-between">
                    <div class="title-box">
                        <h4>Kenapa memilih kami ?</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card-box-c foo">
                    <div class="card-header-c d-flex">
                        <div class="card-box-ico">
                            <span class="fa fa-check text-success"></span>
                        </div>
                        <div class="card-title-c align-self-center">
                            <h4>Kos terjamin</h4>
                        </div>
                    </div>
                    <div class="card-body-c">
                        <p class="content-c">
                            Kos-kosan yang terdaftar pada website. sebelumnya sudah terverifikasi oleh kami. dan
                            keaslian kos terjamin
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-box-c foo">
                    <div class="card-header-c d-flex">
                        <div class="card-box-ico">
                            <span class="fa fa-users text-success"></span>
                        </div>
                        <div class="card-title-c align-self-center">
                            <h4>Berbagai type kos</h4>
                        </div>
                    </div>
                    <div class="card-body-c">
                        <p class="content-c">
                            Terdapat banyak type kos-kosan ada khusus pria, wanita dan bahkan untuk yang sudah
                            berkeluarga.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-box-c foo">
                    <div class="card-header-c d-flex">
                        <div class="card-box-ico">
                            <span class="fa fa-building-o text-success"></span>
                        </div>
                        <div class="card-title-c align-self-center">
                            <h4>Fasilitas</h4>
                        </div>
                    </div>
                    <div class="card-body-c">
                        <p class="content-c">
                            Tersedia fasilitas-fasilitas baik pada kos-kosan yang terdaftar pada website juragan kos.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</section>
<!--/ Services End /-->

<section class="section" style="margin-top: 50px;">
    <div class="container">
        <div class="row">
            <div class="title-box">
                <h4>Kost terdekat ..</h4>
            </div>

            <div class="col-md-12">
                <div id="mapContainer" style="width: 100%;height:500px;"></div>
            </div>

            <!-- <div class="col-md-12" style="margin-top: 20px;">
                <a href="javascript:void(0)" onclick="setPosition(-6.1678108, 106.7910606)" class="btn btn-info btn-sm">Cari sekitar Trisakti</a>
            </div> -->
        </div>
        <div class="row" style="margin-top: 20px;" id="divKost">
            Data Jarak Kost
            <div class="col-md-12">
                <table class="table display" id="tblKost" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nama Kost</th>
                            <th>Jarak (Km)</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for='kost in dataKost'>
                            <td>@{{ kost.nama }}</td>
                            <td>@{{ kost.jarak }}</td>
                            <td>
                                <a href="javascript:void(0)" @click="detailAtc(kost.nama, kost.jarak)" class="btn btn-primary btn-sm">Detail</a>
                            </td>
                            <!-- <td>
                                <a href="javascript:void(0)" class="btn btn-primary" target="new" id="btnLinkDetail">Detail</a>
                            </td> -->
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>

<!--/ kosan terbaru /-->
<section class="section-property section-t8" style="margin-top:200px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-wrap d-flex justify-content-between">
                    <!-- <div class="title-box">
                        <h4>Kos-kosan Terbaru</h4>
                    </div>
                    <div class="title-link">
                        <a href="{{route('object-kos.index')}}">Lihat semua Kosan
                            <span class="ion-ios-arrow-forward"></span>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- <div id="property-carousel" class="owl-carousel owl-theme">
            @foreach($kosan as $kos)
            @php $jmlKamar = $kos->kamar->where('status','tersedia')->count(); @endphp
            @if($jmlKamar > 0)
            <div class="carousel-item-b">
                <div class="rounded card-box-a card-shadow">
                    <div class="img-box-a">
                        <img src="{{asset('backend/image/gambar_kos/'.$kos->gallery->first()->gambar)}}" style="height:400px;" class="w-100 img-a img-fluid">
                    </div>
                    <div class="card-overlay">
                        <div class="card-overlay-a-content">
                            <div class="card-header-a">
                                <h4 class="card-title-a">
                                    <a href="property-single.html">{{$kos->nama_kos}} </a>
                                    <br />
                                    <span class="text-white" style="font-size:12px; ">Lokasi: {{$kos->alamat}}</span>
                                </h4>
                            </div>
                            <div class="card-body-a">
                                <div class="price-box d-flex">
                                    <a href="javascript:void(0)" onclick="openKostDetail('{{ $kos -> latitude }}', '{{ $kos -> longitude }}', '{{ $kos -> id }}')" class="price-a">Detail</a>
                                </div>
                            </div>
                            <div class="card-footer-a">
                                <ul class="card-info d-flex justify-content-around">
                                    <li>
                                        <h4 class="card-info-title">Kamar Kosong</h4>
                                        @php $k = $kos->kamar->where('status','=','tersedia')->count(); @endphp
                                        <span> {{$k}}</span>
                                    </li>
                                    @foreach($kos->fasilitas as $fasilitas)
                                    <li>
                                        <h4 class="card-info-title">
                                            {{$fasilitas->nama_fasilitas == 'penjaga' ? 'Penjaga' : ''}}
                                        </h4>
                                        <span>{{$fasilitas->nama_fasilitas == 'penjaga' ? 'Tersedia' : ''}}</span>
                                    </li>
                                    <li>
                                        <h4 class="card-info-title">
                                            {{$fasilitas->nama_fasilitas == 'garasi' ? 'garasi' : ''}}
                                        </h4>
                                        <span>{{$fasilitas->nama_fasilitas == 'garasi' ? 'Tersedia' : ''}} </span>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div> -->
    </div>
</section>
<!--/ ksoan terbaru /-->

<!-- modal tambah user  -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalDetailKost">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Kost</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Kost</label>
                    <h4 id="capNamaKost"></h4>
                </div>
                <div class="form-group">
                    <label>Tipe Kost</label>
                    <h4 id="capTipeKost"></h4>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <h4 id="capAlamat"></h4>
                </div>
                <div class="form-group">
                    <label>Deksripsi</label>
                    <h4 id="capDeks"></h4>
                </div>
                <div>
                    <a href="javascript:void(0)" class="btn btn-primary" target="new" id="btnLinkDetail">Lihat detail kost</a>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')



<script>
    var latDef = 0;
    var lngDef = 0;
    var platform;
    var defaultLayers;
    var map;
    var kosDipilih = '';

    const apiKey = "BJCLT_PVpaq2HbbHNPBuW-6hgQkLzUpmNIz61fcqZI8";
    const latTri = "-6.1678108";
    const lngTri = "106.7910606";

    getLocation();

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            // x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    var dataJarak = [];
    var dataIdKost = [];
    var dataNamaKost = [];
    var itemsProcessed = 0;

    function showPosition(position) {
        // console.log(position.coords.latitude);
        // console.log(position.coords.longitude);
        // latDef = position.coords.latitude;
        // lngDef = position.coords.longitude;
        latDef = latTri;
        lngDef = lngTri;
        platform = new H.service.Platform({
            'apikey': apiKey
        });
        defaultLayers = platform.createDefaultLayers();
        const defaultPos = {
            lat: latDef,
            lng: lngDef
        }
        map = new H.Map(
            document.getElementById('mapContainer'),
            defaultLayers.vector.normal.map, {
                zoom: 14,
                center: defaultPos,
                pixelRatio: window.devicePixelRatio || 1,
            });


        let ui = H.ui.UI.createDefault(map, defaultLayers);
        let mapEvents = new H.mapevents.MapEvents(map);
        let behavior = new H.mapevents.Behavior(mapEvents);

        var icon = new H.map.Icon('placeholder.png');
        var marker = new H.map.Marker(defaultPos, {
            icon: icon
        });
        map.addObject(marker);

        axios.get("{{ env('APP_URL') }}/api/data-kost/all").then(function(res) {
            // axios.get("{{ env('APP_URL') }}/api/data-kost/getKos/{{$kos->latitude}}/{{$kos->longitude}}").then(function(res) {
            let dataKost = res.data;
            dataKost.forEach(renderKost);
            // console.log(dataKost.length);

            function renderKost(item, index) {
                let namaKost = dataKost[index].nama_kos;
                // let idKost = dataKost[index].id;
                dataIdKost.push(dataKost[index].id);
                dataNamaKost.push(dataKost[index].nama_kos);
                let lat = dataKost[index].latitude;
                let lng = dataKost[index].longitude;
                let iconMarkerKost = new H.map.Icon('hotel.png');
                let posKost = {
                    lat: lat,
                    lng: lng
                }
                let markerKost = new H.map.Marker(posKost, {
                    icon: iconMarkerKost
                });
                map.addObject(markerKost);
                let strLat = lat.toString();
                let strLng = lng.toString();
                let wp1 = strLat + "," + strLng;
                let gabungan = '' + wp1 + '';
                let wpo = latDef + "," + lngDef;
                let asli = "-6.356927058201095,107.17425561491602";
                // route 
                let params = {
                    mode: "fastest;car;traffic:enabled",
                    waypoint0: wpo,
                    waypoint1: gabungan,
                    representation: "display"
                };

                let router = platform.getRoutingService();
                var totalJarak = 0;
                router.calculateRoute(params, (result) => {
                    // console.log(result.response.route[0]);
                    let routeHasil = result.response.route[0];
                    // console.log(routeHasil.leg[0].maneuver);
                    let stepHasil = routeHasil.leg[0].maneuver
                    stepHasil.forEach(renderStep);

                    function renderStep(item, index) {
                        // console.log(stepHasil[index].length);
                        totalJarak = totalJarak + stepHasil[index].length;
                    }
                    var jarakTemp = totalJarak / 1000;
                    dataJarak.push(jarakTemp);
                    let routeLineString = new H.geo.LineString();
                    result.response.route[0].shape.forEach(point => {
                        let [lat, lng] = point.split(",");
                        routeLineString.pushPoint({
                            lat: lat,
                            lng: lng
                        });
                    });
                    let color = ['red', 'blue', 'black', 'aqua', 'silver', 'gold'];
                    let warnaAcak = random_item(color);
                    let routePolyline = new H.map.Polyline(
                        routeLineString, {
                            style: {
                                lineWidth: 5,
                                strokeColor: ''
                            }
                        }
                    );
                    map.addObject(routePolyline);
                });

                itemsProcessed++;
                if (itemsProcessed === dataKost.length) {
                    callback(itemsProcessed);
                }
            }

        });

    }

    function callback(itemsProcessed) {
        console.log('all done');
        setTimeout(function() {
            console.log(dataJarak.length, itemsProcessed);
            // dataNamaKost.forEach(renderTabelKost);
            // var ord = 1;
            for (let i = 0; i < dataNamaKost.length; i++) {
                // console.log(dataJarak[i]);
                let ord = i + 1;
                appKost.dataKost.push({
                    nama: dataNamaKost[i],
                    no: ord,
                    jarak: dataJarak[i]
                });

            }

        }, 4000);

        /** fungsi untuk render datatable */
        setTimeout(function() {
            // if ($.fn.DataTable.isDataTable('#tblKost')) {
            //     $('#tblKost').DataTable().destroy();
            // }
            $("#tblKost").dataTable({
                "order": [
                    [1, "asc"]
                ]
            });
        }, 5000);
    }

    /** fungsi untuk menampilkan table data kost */
    var appKost = new Vue({
        el: '#divKost',
        data: {
            dataKost: [],
            namaKost: ''
        },
        methods: {
            detailAtc: function(kos, jarak) {
                axios.get("{{ env('APP_URL') }}/api/data-kost/detail/" + kos).then(function(res) {
                    // let group = new H.map.Group();
                    // map.removeObject(group);
                    console.log(jarak);
                    document.querySelector("#capNamaKost").innerHTML = res.data.nama_kos;
                    document.querySelector("#capTipeKost").innerHTML = res.data.type_kos;
                    document.querySelector("#capAlamat").innerHTML = res.data.alamat;
                    document.querySelector("#capDeks").innerHTML = res.data.deskripsi_kos;
                    kosDipilih = res.data.slug;
                    let idKost = res.data.id;
                    let strLat = res.data.latitude;
                    let strLng = res.data.longitude;
                    let wp1 = strLat + "," + strLng;
                    let wpo = latDef + "," + lngDef;
                    let gabungan = '' + wp1 + '';
                    let params = {
                        mode: "fastest;car;traffic:enabled",
                        waypoint0: wpo,
                        waypoint1: gabungan,
                        representation: "display"
                    };
                    let router = platform.getRoutingService();


                    router.calculateRoute(params, (result) => {
                        // console.log(result.response.route[0]);
                        let routeHasil = result.response.route[0];
                        // console.log(routeHasil.leg[0].maneuver);
                        let stepHasil = routeHasil.leg[0].maneuver;
                        let routeLineString = new H.geo.LineString();
                        result.response.route[0].shape.forEach(point => {
                            let [lat, lng] = point.split(",");
                            routeLineString.pushPoint({
                                lat: lat,
                                lng: lng
                            });
                        });
                        let color = ['red', 'blue', 'green', 'black', 'aqua', 'silver', 'gold', 'brown, purple', 'navy'];
                        let warnaAcak = random_item(color);
                        // console.log(warnaAcak);
                        let routePolyline = new H.map.Polyline(
                            routeLineString, {
                                style: {
                                    lineWidth: 10,
                                    strokeColor: warnaAcak,
                                    lineDash: [0, 2],
                                    lineTailCap: 'arrow-tail',
                                    lineHeadCap: 'arrow-head'
                                }
                            }
                        );
                        map.addObject(routePolyline);
                        // map.removeObject(router);
                    });
                    // $("#modalDetailKost").modal("show");
                    // https://kos.justhasnah.me/object-kos/9?from=3.6159895834286218,98.75656099929634&to=-6.36198,107.17383
                    // document.querySelector("#btnLinkDetail").setAttribute('href', "{{ env('APP_URL') }}/object-kos/" + idKost + "?from=" + latDef + "," + lngDef + "&to=" + strLat + "," + strLng);
                    // window.open("{{ env('APP_URL') }}/object-kos/" + idKost + "?from=" + latDef + "," + lngDef + "&to=" + strLat + "," + strLng + "&idKost=" + idKost + "&jarak=" + jarak)
                    window.open("{{ env('APP_URL') }}/object-kos/" + idKost + "/" + jarak)

                });

            }
        }
    });

    function random_item(items) {
        return items[Math.floor(Math.random() * items.length)];
    }

    function openKostDetail(latTujuan, lngTujuan, idKost) {
        let linkOpen = server + "object-kos/" + idKost + "?from=" + latDef + "," + lngDef + "&to=" + latTujuan + "," + lngTujuan;
        window.open(linkOpen);
        // console.log(linkOpen);
    }


    /**fungsi untuk cari sekitar trisakti */
    function setPosition(lat = null, long = null) {
        document.getElementById('mapContainer').innerHTML = '';
        // console.log(lat, long)
        latDef = lat;
        lngDef = long;
        platform = new H.service.Platform({
            'apikey': apiKey
        });
        defaultLayers = platform.createDefaultLayers();
        const defaultPos = {
            lat: latDef,
            lng: lngDef
        }
        map = new H.Map(
            document.getElementById('mapContainer'),
            defaultLayers.vector.normal.map, {
                zoom: 14,
                center: defaultPos,
                pixelRatio: window.devicePixelRatio || 1,
            });


        let ui = H.ui.UI.createDefault(map, defaultLayers);
        let mapEvents = new H.mapevents.MapEvents(map);
        let behavior = new H.mapevents.Behavior(mapEvents);

        var icon = new H.map.Icon('placeholder.png');
        var marker = new H.map.Marker(defaultPos, {
            icon: icon
        });
        map.addObject(marker);
        let dataJarak2 = [];
        let dataIdKost = [];
        let dataNamaKost = [];
        let itemsProcessed = 0;

        axios.get("{{ env('APP_URL') }}/api/data-kost/all").then(function(res) {
            let dataKost = res.data;
            dataKost.forEach(renderKost);

            function renderKost(item, index) {
                let namaKost = dataKost[index].nama_kos;
                // let idKost = dataKost[index].id;
                dataIdKost.push(dataKost[index].id);
                dataNamaKost.push(dataKost[index].nama_kos);
                let lat = dataKost[index].latitude;
                let lng = dataKost[index].longitude;
                let iconMarkerKost = new H.map.Icon('hotel.png');
                let posKost = {
                    lat: lat,
                    lng: lng
                }
                let markerKost = new H.map.Marker(posKost, {
                    icon: iconMarkerKost
                });
                map.addObject(markerKost);
                let strLat = lat.toString();
                let strLng = lng.toString();
                let wp1 = strLat + "," + strLng;
                let gabungan = '' + wp1 + '';
                let wpo = latDef + "," + lngDef;
                let asli = "-6.356927058201095,107.17425561491602";
                // route 
                let params = {
                    mode: "fastest;car;traffic:enabled",
                    waypoint0: wpo,
                    waypoint1: gabungan,
                    representation: "display"
                };

                let router = platform.getRoutingService();
                var totalJarak = 0;
                router.calculateRoute(params, (result) => {
                    // console.log(result.response.route[0]);
                    let routeHasil = result.response.route[0];
                    // console.log(routeHasil.leg[0].maneuver);
                    let stepHasil = routeHasil.leg[0].maneuver
                    stepHasil.forEach(renderStep);

                    function renderStep(item, index) {
                        // console.log(stepHasil[index].length);
                        totalJarak = totalJarak + stepHasil[index].length;
                    }
                    var jarakTemp = totalJarak / 1000;
                    dataJarak2.push(jarakTemp);
                    let routeLineString = new H.geo.LineString();
                    result.response.route[0].shape.forEach(point => {
                        let [lat, lng] = point.split(",");
                        routeLineString.pushPoint({
                            lat: lat,
                            lng: lng
                        });
                    });
                    let color = ['red', 'blue', 'black', 'aqua', 'silver', 'gold'];
                    let warnaAcak = random_item(color);
                    let routePolyline = new H.map.Polyline(
                        routeLineString, {
                            style: {
                                lineWidth: 5,
                                strokeColor: 'green'
                            }
                        }
                    );
                    map.addObject(routePolyline);
                });

                itemsProcessed++;
            }
            // console.log(itemsProcessed, dataJarak2)
            setTimeout(function() {
                appKost.dataKost = [];
                for (let i = 0; i < dataNamaKost.length; i++) {
                    let ord = i + 1;
                    appKost.dataKost.push({
                        nama: dataNamaKost[i],
                        no: ord,
                        jarak: dataJarak2[i]
                    });

                }

            }, 5000);

        });
    }
</script>

@endpush