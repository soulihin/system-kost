@extends('layouts.dashboard')

@section('title')
    Gambar Kamar Kos
@stop

@section('content')
<section class="section">
  <div class="section-header">
    <div class="section-header-breadcrumb">
      <div class="breadcrumb active"><a href="{{route('home')}}">Dashboard</a></div>
      <div class="breadcrumb active"><a href="{{ route('kos.edit',$kamar->kos->id) }}">{{$kamar->kos->nama_kos}}</a></div>
      <div class="breadcrumb">ID kamar - {{$kamar->id}}</div>
    </div>
  </div>
  <div class="section-body">
    <div class="row ">
    
    @if(session('status'))
        <div class="alert alert-warning" role="alert">
            {{session('status')}}
        </div>
    @endif
      <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Tambahkan Gambar Untuk Kamar ID - {{$kamar->id}}</div>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach ($kamar->galleryKamar as $gallery)
                    <div class="col-md-4">
                        <div class="gallery-container">
                            <!-- <img src="{{ Storage::url($gallery->gambar) }}" class="w-100"> -->
                            <a href="{{ route('gallery-kamar.delete',$gallery->id) }}" class="delete-gallery">
                                <img src="{{ asset('backend/assets/img/icon-delete.svg') }}">
                            </a>
                            <img src="{{ asset('backend/image/galeri_kamar/'.$gallery->gambar) }}" class="w-100" style="width:40px;">
                        </div>
                    </div>
                    @endforeach
                    <div class="col-12">
                        <form action="{{route('gambar-kamar.store')}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            @error('gambar')
                            <span class="invalid-feedback">
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            </span>
                            @enderror
                            <input type="hidden" name="kamar_id" value="{{ $kamar->id }}">
                            <input name="gambar" class="form-control" type="file" id="file">
                            <button class="mt-3 btn btn-secondary btn-block" type="submit">
                                Tambahkan Gambar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop