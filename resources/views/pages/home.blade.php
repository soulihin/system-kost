@extends('layouts.landingPage')

@section('title')
Home
@endsection

@section('content')
<style>
    .text-brand {
        color: #fff !important;
    }

    .nav-link {
        color: #fff !important;
    }
</style>
<!--/ Carousel Star /-->

<section class="section-services section-t8">

</section>
<!--/ Services End /-->

<section class="section" style="margin-top: 50px;">
    <div class="container">
        <div class="row">
            <div class="title-box">
                <h4>Kost terdekat ..</h4>
            </div>

            <div class="col-md-12">
                <div id="mapContainer" style="width: 100%;height:500px;"></div>
            </div>

        </div>
        <div class="row" style="margin-top: 20px;" id="divKost">
            Data Jarak Kost
            <div class="col-md-12">
                <table class="table display" id="tblKost" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nama Kost</th>
                            <th>Jarak (Km)</th>
                            <th>Harga (Rp)</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for='kost in dataKost'>
                            <td>@{{ kost.nama }}</td>
                            <td>@{{ kost.jarak }}</td>
                            <td>@{{ kost.harga }}</td>
                            <td>
                                <a href="javascript:void(0)" @click="detailAtc(kost.nama, kost.jarak, biaya_booking)" class="btn btn-primary btn-sm">Detail</a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>



<!-- modal tambah user  -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalDetailKost">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Kost</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Kost</label>
                    <h4 id="capNamaKost"></h4>
                </div>
                <div class="form-group">
                    <label>Tipe Kost</label>
                    <h4 id="capTipeKost"></h4>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <h4 id="capAlamat"></h4>
                </div>
                <div class="form-group">
                    <label>Deksripsi</label>
                    <h4 id="capDeks"></h4>
                </div>
                <div>
                    <a href="javascript:void(0)" class="btn btn-primary" target="new" id="btnLinkDetail">Lihat detail kost</a>
                </div>
            </div>

        </div>
    </div>
</div>


@endsection

@push('scripts')



<script>
    var latDef = 0;
    var lngDef = 0;
    var platform;
    var defaultLayers;
    var map;
    var kosDipilih = '';

    const apiKey = "BJCLT_PVpaq2HbbHNPBuW-6hgQkLzUpmNIz61fcqZI8";
    const latTri = "-6.1678108";
    const lngTri = "106.7910606";

    getLocation();

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            // x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }


    var dataJarak = [];
    var dataIdKost = [];
    var dataNamaKost = [];
    var itemsProcessed = 0;


    function showPosition(position) {


        latDef = latTri;
        lngDef = lngTri;

        platform = new H.service.Platform({
            'apikey': apiKey,
        });
        defaultLayers = platform.createDefaultLayers();
        const defaultPos = {
            lat: latDef,
            lng: lngDef
        }

        map = new H.Map(
            document.getElementById('mapContainer'),
            defaultLayers.vector.normal.map, {
                zoom: 15,
                center: defaultPos,
                pixelRatio: window.devicePixelRatio || 1,
            });

        let ui = H.ui.UI.createDefault(map, defaultLayers);
        let mapEvents = new H.mapevents.MapEvents(map);
        let behavior = new H.mapevents.Behavior(mapEvents);


        var icon = new H.map.Icon('placeholder.png');
        var marker = new H.map.Marker(defaultPos, {
            icon: icon
        });
        map.addObject(marker);


        axios.get("{{ env('APP_URL') }}/api/data-kost/all").then(function(res) {
            let dataKost = res.data;


            dataKost.forEach(renderKost);


            function renderKost(item, index) {

                let lat = dataKost[index].latitude;
                let lng = dataKost[index].longitude;

                let iconMarkerKost = new H.map.Icon('hotel.png');

                let posKost = {
                    lat: lat,
                    lng: lng
                }
                let markerKost = new H.map.Marker(posKost, {
                    icon: iconMarkerKost
                });
                map.addObject(markerKost);


                let strLat = lat.toString();
                let strLng = lng.toString();
                let wp1 = strLat + "," + strLng;
                let gabungan = '' + wp1 + '';
                let wpo = latDef + "," + lngDef;


                let params = {
                    mode: "shortest;car;",
                    waypoint0: wpo,
                    waypoint1: gabungan,
                    representation: "display"
                };


                let router = platform.getRoutingService();
                var totalJarak = 0;
                /** proses bfs */
                router.calculateRoute(params, (result) => {

                    let routeHasil = result.response.route[0];
                    let stepHasil = routeHasil.leg[0].maneuver
                    stepHasil.forEach(renderStep);

                    function renderStep(item, index) {
                        totalJarak = totalJarak + stepHasil[index].length;
                    }
                    var jarakTemp = totalJarak / 1000;

                    dataJarak.push({
                        nama: dataKost[index].nama_kos,
                        jarak: jarakTemp,
                        id: dataKost[index].id,
                        harga: dataKost[index].kamar[0].biaya_perbulan
                    });


                    let routeLineString = new H.geo.LineString();
                    result.response.route[0].shape.forEach(point => {
                        let [lat, lng] = point.split(",");
                        routeLineString.pushPoint({
                            lat: lat,
                            lng: lng
                        });
                    });

                    let routePolyline = new H.map.Polyline(
                        routeLineString, {
                            style: {
                                lineWidth: 5,
                                strokeColor: ''
                            }
                        }
                    );
                    map.addObject(routePolyline);
                });


                itemsProcessed++;
                if (itemsProcessed === dataKost.length) {

                    callback(itemsProcessed);
                }
            }

        });

    }

    function callback(itemsProcessed) {
        setTimeout(function() {
            console.log(dataJarak);
            for (let i = 0; i < dataJarak.length; i++) {
                let ord = i + 1;
                appKost.dataKost.push({
                    nama: dataJarak[i]['nama'],
                    no: ord,
                    jarak: dataJarak[i]['jarak'],
                    harga: Currency(dataJarak[i]['harga'])
                });
            }
        }, 5000);


        setTimeout(function() {
            $("#tblKost").dataTable({
                "order": [
                    [1, "asc"]
                ]
            });
        }, 5000);
    }


    var appKost = new Vue({
        el: '#divKost',
        data: {
            dataKost: [],
            namaKost: ''
        },
        methods: {
            detailAtc: function(kos, jarak) {
                axios.get("{{ env('APP_URL') }}/api/data-kost/detail/" + kos).then(function(res) {
                    // let group = new H.map.Group();

                    // map.removeObject(group);
                    console.log(jarak);
                    document.querySelector("#capNamaKost").innerHTML = res.data.nama_kos;
                    document.querySelector("#capTipeKost").innerHTML = res.data.type_kos;
                    document.querySelector("#capAlamat").innerHTML = res.data.alamat;
                    document.querySelector("#capDeks").innerHTML = res.data.deskripsi_kos;
                    kosDipilih = res.data.slug;

                    let idKost = res.data.id;
                    let strLat = res.data.latitude;
                    let strLng = res.data.longitude;
                    let wp1 = strLat + "," + strLng;
                    let wpo = latDef + "," + lngDef;
                    let gabungan = '' + wp1 + '';
                    let params = {
                        mode: "shortest;car;",
                        waypoint0: wpo,
                        waypoint1: gabungan,
                        representation: "display"
                    };
                    let router = platform.getRoutingService();


                    router.calculateRoute(params, (result) => {
                        // console.log(result.response.route[0]);
                        let routeHasil = result.response.route[0];
                        // console.log(routeHasil.leg[0].maneuver);
                        let stepHasil = routeHasil.leg[0].maneuver;
                        let routeLineString = new H.geo.LineString();
                        result.response.route[0].shape.forEach(point => {

                            let [lat, lng] = point.split(",");
                            routeLineString.pushPoint({
                                lat: lat,
                                lng: lng
                            });
                        });
                        let color = ['red', 'blue', 'green', 'black', 'aqua', 'gold', 'purple', 'navy'];
                        let warnaAcak = random_item(color);
                        // console.log(warnaAcak);
                        let routePolyline = new H.map.Polyline(
                            routeLineString, {
                                style: {
                                    lineWidth: 10,
                                    strokeColor: warnaAcak,
                                    lineDash: [0, 2],
                                    lineTailCap: 'arrow-tail',
                                    lineHeadCap: 'arrow-head'
                                }
                            }
                        );
                        map.addObject(routePolyline);
                        // map.removeObject(router);
                    });

                    window.open("{{ env('APP_URL') }}/object-kos/" + idKost + "/" + jarak)

                });

            }
        }
    });

    function random_item(items) {
        return items[Math.floor(Math.random() * items.length)];
    }

    function openKostDetail(latTujuan, lngTujuan, idKost) {
        let linkOpen = server + "object-kos/" + idKost + "?from=" + latDef + "," + lngDef + "&to=" + latTujuan + "," + lngTujuan;
        window.open(linkOpen);
        // console.log(linkOpen);
    }



    function setPosition(lat = null, long = null) {
        document.getElementById('mapContainer').innerHTML = '';

    }

    function Currency(nStr) {
        let val = (nStr / 1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        // return (nStr).toString().replace(/\d(?=(\d{3})+\.)/g, '$&,');
    }
</script>

@endpush