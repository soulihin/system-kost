@extends('layouts.landingPage')

@section('title')
Semua Kos
@stop

@section('content')
<style>
    .text-brand {
        color: #fff !important;
    }

    .nav-link {
        color: #fff !important;
    }
</style>
<section class="intro-single">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="title-single-box">
                    <h6>Semua Kos-kosan Usakti Kos</h6>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{route('object-kos.index')}}">Semua Kos</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
        <!-- <div id="mapContainer" style="width: 640px; height: 480px"></div> -->
    </div>
</section>
<!--  -->
<section class="grid property-grid">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="grid-option">
                    <!-- <form action="{{route('object-kos.index')}}">
                        <input type="text" name="keyword" placeholder="Cari alamat kos..">
                    </form> -->
                </div>
            </div>
            @foreach($kosan as $kos)
            @php $jmlKamar = $kos->kamar->where('status','tersedia')->count(); @endphp
            @if($jmlKamar > 0)
            <div class="col-md-4">
                <div class="card-box-a card-shadow">
                    <div class="img-box-a">
                        <img src="{{ asset('backend/image/gambar_kos/'.$kos->gallery->first()->gambar )}}" style="height:400px;" class="w-100 img-a img-fluid">
                    </div>
                    <div class="card-overlay">
                        <div class="card-overlay-a-content">
                            <div class="card-header-a">
                                <h2 class="card-title-a">
                                    <a href="{{ env('APP_URL') }}/object-kos/{{ $kos -> id }}/0" target="_blank">{{$kos->nama_kos}} </a>
                                    <br />
                                    <span class="text-white" style="font-size:12px; ">Lokasi: {{$kos->alamat}}</span>
                                </h2>
                            </div>
                            <div class="card-body-a">
                                <!-- <div class="price-box d-flex"> -->
                                <!-- <a class="price-a" href="{{route('kos.detail',$kos->slug)}}">
                                    Detail
                                </a> -->
                                <!-- <a href="javascript:void(0)" onclick="openKostDetail('{{ $kos -> latitude }}', '{{ $kos -> longitude }}', '{{ $kos -> id }}')" class="price-a">Detail</a> -->
                                <!-- <a href="javascript:void(0)" class="price-a"></a> -->
                                <!-- </div> -->
                            </div>
                            <div class="card-footer-a">
                                <ul class="card-info d-flex justify-content-around">
                                    <li>
                                        <a href="{{ env('APP_URL') }}/object-kos/{{ $kos -> id }}/0" target="_blank">
                                            <h4 class="card-info-title">Lihat Detail</h4>
                                        </a>
                                        <!-- <h4 class="card-info-title">Kamar Kosong</h4>
                                        @php $k = $kos->kamar->where('status','=','tersedia')->count(); @endphp
                                        <span> {{$k}}</span> -->
                                    </li>
                                    <!-- @foreach($kos->fasilitas as $fasilitas)
                                    <li>
                                        <h4 class="card-info-title">
                                            {{$fasilitas->nama_fasilitas == 'penjaga' ? 'Penjaga' : ''}}
                                        </h4>
                                        <span>{{$fasilitas->nama_fasilitas == 'penjaga' ? 'Tersedia' : ''}}</span>
                                    </li>
                                    <li>
                                        <h4 class="card-info-title">
                                            {{$fasilitas->nama_fasilitas == 'garasi' ? 'garasi' : ''}}
                                        </h4>
                                        <span>{{$fasilitas->nama_fasilitas == 'garasi' ? 'Tersedia' : ''}} </span>
                                    </li>
                                    @endforeach -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
        <div class="mt-3 row">
            <div class="text-center col-12">
                {{$kosan->links()}}
            </div>
        </div>
    </div>
</section>
@stop
@push('scripts')
<script>
    var latDef = 0;
    var lngDef = 0;
    var platform;
    var defaultLayers;
    var map;

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            // x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        latDef = position.coords.latitude;
        lngDef = position.coords.longitude;
    }

    getLocation();

    function openKostDetail(latTujuan, lngTujuan, idKost) {
        let linkOpen = server + "object-kos/" + idKost + "?from=" + latDef + "," + lngDef + "&to=" + latTujuan + "," + lngTujuan;
        window.open(linkOpen);
        // console.log(linkOpen);
    }
</script>

@endpush