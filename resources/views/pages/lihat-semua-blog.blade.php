@extends('layouts.landingPage')

@section('title')
    Cari Kosan
@endsection

@section('content')
  <!--/ Intro Single star /-->
  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h6> Kosan Terdekat</h6>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{{url('/')}}">Home</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Kosan Terdekat
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->

  <!--/ News Grid Star /-->
  <section class="grid mb-5 news-grid">
    <div class="container">
      <div class="row">
        <!-- @foreach($blogs as $blog) -->
        <div class="col-md-4">
          <div class="card-box-b card-shadow news-box">
          <div id="map" class="contact-map">
              <div style="height: 500px" id="mapContainer"></div>
            <div id="summary"></div>
          </div>
          </div>
        </div>
        <!-- @endforeach -->
      </div>
    </div>
  </section>
  <!--/ News Grid End /-->
@endsection