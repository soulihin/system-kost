@extends('layouts.landingPage')

@section('title')
Detail Kos {{$kos->slug}}
@endsection

@section('content')
<style>
    .rating-css div {
        color: #ffe400;
        font-size: 30px;
        font-family: sans-serif;
        font-weight: 800;
        text-align: center;
        text-transform: uppercase;
        padding: 20px 0;
    }

    .rating-css input {
        display: none;
    }

    .rating-css input+label {
        font-size: 50px;
        text-shadow: 1px 1px 0 #8f8420;
        cursor: pointer;
    }

    .rating-css input:checked+label~label {
        color: #b4afaf;
    }

    .rating-css label:active {
        transform: scale(0.8);
        transition: 0.3s ease;
    }

    .checked {
        color: #ffd900;
    }

    .text-brand {
        color: #fff !important;
    }

    .nav-link {
        color: #fff !important;
    }
</style>
<!--/ Intro Single star /-->
<section class="intro-single">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="title-single-box">

                    <h4 class="">{{$kos->nama_kos}} -
                        <!-- @php
            $ratenum = number_format($rating_value);
            @endphp
            @for($i = 1; $i <= $ratenum; $i++) <i style="font-size: 20px;" class="fa fa-star checked"></i>
              @endfor
              @for($j = $ratenum+1; $j <= 5; $j++) <i style="font-size: 20px;" class="fa fa-star"></i>
                @endfor
                <small style="font-weight:light; font-size:18px;" class="text-muted">
                  ( {{$ratings->count()}} )
                </small> -->
                    </h4>
                    <span class="color-text-a">{{$kos->alamat}}</span>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            {{$kos->nama_kos}}
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!--/ Intro Single End /-->

<!--/ Property Single Star /-->
<section class="property-single nav-arrow-b">
    <div class="container">
        <div class="row">
            <div class="mb-4 col-sm-12">
                <div id="property-single-carousel" class="owl-carousel owl-arrow gallery-property">
                    <!-- <div class="carousel-item-b">
              <img src="{{Storage::url($kos->gallery->first()->gambar ?? '')}}" alt="" style="height: 500px;">
            </div> -->

                    @foreach($kos->gallery as $gallery)
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{ asset('backend/image/gambar_kos/' . $gallery->gambar) }}" alt="" style="height: 500px;">
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="row justify-content-between">
                    <div class="col-md-5 col-lg-4">
                        <div class="property-price ">
                            @auth
                            <form action="{{route('kos-tersimpan.store')}}" method="post">
                                @csrf
                                <input type="hidden" name="kos_id" value="{{$kos->id}}">
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <button type="submit" class="btn btn-block btn-sm btn-success"><i class="fa fa-list"></i> Simpan</button>
                            </form>
                            @else
                            <a href="{{route('login')}}" onClick="return confirm('Silahkan Login Dulu')" type="submit" class="btn btn-block btn-sm btn-light"><i class="fa fa-heart"></i> Simpan</a>
                            @endauth
                        </div>
                        <div class="property-summary">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="title-box-d section-t4">
                                        <h4>Informasi Kos</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="summary-list">
                                <ul class="list">
                                    <!-- <li class="d-flex justify-content-between">
                    <strong>Kos ID:</strong>
                    <span>{{$kos->id}}</span>
                  </li> -->
                                    <li class="d-flex justify-content-between">
                                        <strong>Alamat:</strong>
                                        <span>{{$kos->alamat}}</span>
                                    </li>
                                    <li class="d-flex justify-content-between">
                                        <strong>Booking:</strong>
                                        @if($kos->is_booking == 1 )
                                        <span>Tersedia</span>
                                        @else
                                        <span>Tidak Tersedia</span>
                                        @endif
                                    </li>
                                    @if($kos->biaya_booking)
                                    <li class="d-flex justify-content-between">
                                        <strong>Biaya Boking:</strong>
                                        <span>{{$kos->biaya_booking}}</span>
                                    </li>
                                    @endif
                                    <li class="d-flex justify-content-between">
                                        <strong>Jenis Kos:</strong>
                                        <span>{{$kos->type_kos}}</span>
                                    </li>
                                    <li class="d-flex justify-content-between">
                                        <strong>Aturan Kos:</strong>
                                        <span>{{$kos->aturan_kos}}</span>
                                    </li>
                                    <li class="d-flex justify-content-between">
                                        <strong>Area:</strong>
                                        <span>{{$kos->luas_kos}}
                                        </span>
                                    </li>
                                    <li class="d-flex justify-content-between">
                                        <strong>Kamar:</strong>
                                        <span>{{$kos->kamar->count()}}</span>
                                    </li>
                                    <li class="d-flex justify-content-between">
                                        <strong>Pemilik Kos:</strong>
                                        <span>{{$kos->user->name}}</span>
                                    </li>
                                    <li class="d-flex justify-content-between">
                                        <strong>Kontak Pemilik:</strong>
                                        <span>{{$kos->user->no_hp}}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-lg-7 section-md-t3">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="title-box-d">
                                    <h4>Tentang Kos</h4>
                                </div>
                            </div>
                        </div>
                        <div class="property-description">
                            <p class="description color-text-a">
                                {{$kos->deskripsi_kos}}
                            </p>
                        </div>
                        <div class="row section-t3">
                            <div class="col-sm-12">
                                <div class="title-box-d">
                                    <h4>Fasilitas Kos</h4>
                                </div>
                            </div>
                        </div>
                        <div class="amenities-list color-text-a">
                            <ol class="list-b no-margin">
                                @foreach($kos->fasilitas as $fasilitas)
                                <li>{{$fasilitas->nama_fasilitas}}</li> &middot;
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- pilihan kamar -->
            <div class="container col-md-12">
                <div class="row section-t3">
                    <div class="col-sm-12">
                        <div class="title-box-d">
                            <h4>Pilihan Kamar</h4>
                        </div>
                    </div>
                </div>
                @forelse($kos->kamar as $kamar)
                <div class="mb-5 shadow card">
                    <div class="card-body">
                        <div class="mb-5 row">
                            <div class="col-md-6 col-lg-5">
                                <!-- <div class="carousel-item active">
                          <img class="d-block w-100" src="{{ Storage::url($kamar->galleryKamar->first()->gambar ?? '') }}" alt="First slide">
                        </div> -->
                                @foreach ($kamar->galleryKamar as $gallery)
                                <div>
                                    <img class="d-block" style="height: 350px; width: 250px;" src="{{ asset('backend/image/galeri_kamar/'.$gallery->gambar) }}" alt="">
                                </div>
                                @endforeach
                                <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon text-dark" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <div class="col-md-6 col-lg-5">
                                <!--<div class="mb-4">
                                    <!-- <h3>Kamar ID : {{$kamar->id}}</h3>
                                    <div class="px-4 btn btn-info disabled">
                                        Kosong
                                    </div>
                                </div> -->
                                <hr>
                                <div class="text-muted">
                                    <div class="row">
                                        <div class="col-5">
                                            Jumlah Kasur
                                        </div>
                                        <div class="col-2">
                                            :
                                        </div>
                                        <div class="col-2">
                                            {{$kamar->jumlah_kasur}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            Luas Kamar
                                        </div>
                                        <div class="col-2">
                                            :
                                        </div>
                                        <div class="col-4">
                                            {{$kamar->luas_kamar}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            Ukuran kamar
                                        </div>
                                        <div class="col-2">
                                            :
                                        </div>
                                        <div class="col-2">
                                            {{$kamar->ukuran_kamar}}
                                        </div>
                                    </div>
                                </div>
                                <div class="container mt-5 row">
                                    <p class="text-muted" style="font-weight: bold;">Fasilitas kamar &nbsp; : &nbsp;</p>
                                    <div class="col-12 amenities-list color-text-a">
                                        <ul>
                                            @foreach($kamar->fasilitas as $fasilitas)
                                            <li>{{$fasilitas->nama_fasilitas}} </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center text-white btn-block btn-sm btn-warning">
                            Rp. {{number_format($kamar->biaya_perbulan)}} / <span class="text-white b;l t6yukaq ">Bulan</span>
                        </div>
                        @auth
                        @if($kos->is_booking == 1)
                        <!--<a href="{{url('booking/' . $kos->id . '/' . $kamar->id . '/')}}" class="text-center btn-block btn-sm btn-primary">
                            <span class="text-white">Booking</span>-->
                        </a>
                        @endif
                        @else

                        <!--<a class="text-center btn-block btn-sm btn-primary" href="{{route('login')}}" onClick="return confirm('Anda Harus Login Dulu.')">
                            <span class="text-white">Booking</span>
                        </a>-->

                        @endauth
                    </div>
                </div>
                @empty
                <div class="row">
                    <div class="text-center col-12 alert alert-warning"> Kamar Tidak Tersedia</div>
                </div>
                @endforelse
            </div>
            <!-- end pilihan kamar -->
        </div>
        <!-- maps -->
        <section class="row">
            <div class="col-md-12">
                <div class="shadow card">
                    <div class="card-body">
                        <div class="contact-map box">
                            <div id="" class="contact-map">
                                <div style="height: 500px" id="mapContainerDetail"></div>

                                <div class="col-md-12">
                                    <!-- <div id="mapContainer" style="width: 100%;height:500px;"></div> -->
                                </div>
                                <h6>{{ $kos->nama_kos }}</h6>
                                <span>{{ $kos->alamat_kos }}</span>
                                <div id="summary"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><br>
            <div class="col-md-12">
                <!-- <a href="javascript:void(0)" onclick="setPosition(-6.1678108, 106.7910606)" class="btn btn-info btn-sm">Cari sekitar Trisakti</a> -->
                <!-- <a href="javascript:void(0)" onclick="setPosition(latDef, lngDef)" class="btn btn-info btn-sm">Menuju Lokasi
                    Kosan</a> -->
            </div>
            <!-- <div class="col-md-12" style="margin-top: 20px;" id="divKost">
                Jarak Kost Sesuai Rute ke {{ $kos->nama_kos }}
                <div class="col-md-12">
                    <table class="table display" id="tblKost" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama Kost</th>
                                <th>Jarak (Km)</th>
                                <th>Aksi</th>
                            </tr> -->
            <!-- </thead> -->
            <!-- <tbody> -->
            <!-- <tr v-for='kost in dataKost'> -->
            <!-- <td>@{{ kost.nama }}</td> -->
            <!-- <td>@{{ kost.jarak }}</td> -->
            <!-- <td>
                                    <a href="javascript:void(0)" @click="detailAtc(kost.nama, kost.jarak)" class="btn btn-primary btn-sm">Detail</a>
                                </td> -->
            <!-- </tr> -->
            <!-- </tbody> -->
            <!-- </table> -->
            <!-- </div> -->
            <!-- </div> -->
        </section>
        <!-- end masp -->
    </div>
</section>
<!--/ Property Single End /-->

<!-- testimonial -->
<div class="row" style="margin-bottom:50px;">
    <div class="container">
        <hr>
        <h6 style="font-style:italic;" class="mb-4">Silahkan Klik tombol dibawah untuk Hubungi kami !</h6>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                @forelse($kos->testimonial as $testimonial)
                <div class="row">
                    <div class="col-1">
                        @if ($testimonial->user->avatar)
                        <img src="{{ Storage::url($testimonial->user->avatar) }}" class="mr-1 rounded-circle" height="60">
                        @else
                        <img src="https://ui-avatars.com/api/?name={{ $testimonial->user->name }}" height="60" class="mr-1 rounded-circle" />
                        @endif
                    </div>
                    <div class="col">
                        @for($i = 1; $i <= $testimonial->stars_rated; $i++)
                            <i style="font-size: 10px;" class="fa fa-star checked"></i>
                            @endfor
                            <p>"{{$testimonial->testimonial}}"<br><span style="font-style:normal;">- {{$testimonial->user->name}}</span></p>
                    </div>
                </div>
                @empty
                <div class="col">
                    <a href="https://wa.me/{{ $kos->user->no_hp }}" class="btn btn-success btn-lg" target="_blank"> Hubungi Kami</a>
                </div>
                <!-- <div class="text-center col-12 alert alert-warning">
                    Belum Ada Testimonial
              </div> -->
                @endforelse
            </div>
        </div>
    </div>
</div>
<!-- end testimonial -->


@endsection


@push('scripts')
<!-- <script>
  window.action = "direction"
</script> -->

<script>
    var latDef = 0;
    var lngDef = 0;
    var platform;
    var defaultLayers;
    var map;
    var kosDipilih = '';

    const apiKey = "BJCLT_PVpaq2HbbHNPBuW-6hgQkLzUpmNIz61fcqZI8";
    const base_url = "{{ env('APP_URL ') }}";
    const latTri = "-6.1678108";
    const lngTri = "106.7910606";

    getLocation();

    /** fungsi untuk get posisi kita di maps */
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            // x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    /** ambil latlong device */
    function showPosition(position) {
        // console.log(position.coords.latitude);
        // console.log(position.coords.longitude);
        // latDef = position.coords.latitude;
        // lngDef = position.coords.longitude;

        /** ambil latlong trisakti */
        latDef = latTri;
        lngDef = lngTri;
        /** set api key */
        platform = new H.service.Platform({
            'apikey': apiKey
        });
        defaultLayers = platform.createDefaultLayers();
        const defaultPos = {
            lat: latDef,
            lng: lngDef
        }
        /** configurasi map */
        map = new H.Map(
            document.getElementById('mapContainerDetail'),
            defaultLayers.vector.normal.map, {
                zoom: 14,
                center: defaultPos,
                pixelRatio: window.devicePixelRatio || 1,
            });

        let ui = H.ui.UI.createDefault(map, defaultLayers);
        let mapEvents = new H.mapevents.MapEvents(map);
        let behavior = new H.mapevents.Behavior(mapEvents);

        /** set custom icon */
        var icon = new H.map.Icon(base_url + "/placeholder.png");
        var marker = new H.map.Marker(defaultPos, {
            icon: icon
        });
        map.addObject(marker);

        /** set default data jarak */
        var dataJarak = [];
        var dataIdKost = [];
        var dataNamaKost = [];

        /** get data from rest api */
        axios.get("{{ env('APP_URL') }}/api/data-kost/detail_id/{{$kos_id}}").then(function(res) {
            // axios.get("{{ env('APP_URL') }}/api/data-kost/getKos/{{$kos->latitude}}/{{$kos->longitude}}").then(function(res) {
            let dataKost = res.data;
            /** proses render data ke map, menjalankan function renderKost */
            dataKost.forEach(renderKost);

            /** set custom icon titik awal */
            let iconMarkerTrisakti = new H.map.Icon(base_url + "/trisakti.png");
            let setKost = {
                lat: latTri,
                lng: lngTri
            }
            let markerKost2 = new H.map.Marker(setKost, {
                icon: iconMarkerTrisakti
            });
            map.addObject(markerKost2);

            /** fungsi untuk proses render data ke map */
            function renderKost(item, index) {
                /** ambil data lat long dari data api */
                let namaKost = dataKost[index].nama_kos;
                dataIdKost.push(dataKost[index].id);
                dataNamaKost.push(dataKost[index].nama_kos);
                let lat = dataKost[index].latitude;
                let lng = dataKost[index].longitude;
                /** set custom icon titik awal */
                let iconMarkerKost = new H.map.Icon(base_url + "/hotel.png");
                let posKost = {
                    lat: lat,
                    lng: lng
                }
                let markerKost = new H.map.Marker(posKost, {
                    icon: iconMarkerKost
                });
                map.addObject(markerKost);

                /** cari wp2 (harusnya dinamis) */
                // let lat2 = "-6.16495";
                // let lng2 = "106.79165";
                // let iconMarkerKost2 = new H.map.Icon(base_url + "/hotel.png");
                // let posKost2 = {
                //   lat: lat2,
                //   lng: lng2
                // }
                // let markerKost2 = new H.map.Marker(posKost2, {
                //   icon: iconMarkerKost2
                // });
                // map.addObject(markerKost2);
                // let strLat2 = lat2.toString();
                // let strLng2 = lng2.toString();
                // let wp2 = strLat2 + "," + strLng2;

                let strLat = lat.toString();
                let strLng = lng.toString();
                let wp1 = strLat + "," + strLng;
                let gabungan = '' + wp1 + '';
                let wpo = latDef + "," + lngDef;
                let asli = "-6.356927058201095,107.17425561491602";
                // route 
                let params = {
                    mode: "fastest;car;traffic:enabled",
                    waypoint0: wpo,
                    waypoint1: wp1,
                    // waypoint2: wp2,
                    representation: "display"
                };

                let router = platform.getRoutingService();
                var totalJarak = 0;
                router.calculateRoute(params, (result) => {
                    // console.log(result.response.route[0]);
                    let routeHasil = result.response.route[0];
                    // console.log(routeHasil.leg[0].maneuver);
                    let stepHasil = routeHasil.leg[0].maneuver
                    stepHasil.forEach(renderStep);

                    function renderStep(item, index) {
                        // console.log(stepHasil[index].length);
                        totalJarak = totalJarak + stepHasil[index].length;
                    }
                    var jarakTemp = totalJarak / 1000;
                    dataJarak.push(jarakTemp);
                    let routeLineString = new H.geo.LineString();
                    result.response.route[0].shape.forEach(point => {
                        let [lat, lng] = point.split(",");
                        routeLineString.pushPoint({
                            lat: lat,
                            lng: lng
                        });
                    });
                    let color = ['red', 'blue', 'black', 'aqua', 'silver', 'gold'];
                    let warnaAcak = random_item(color);
                    let routePolyline = new H.map.Polyline(
                        routeLineString, {
                            style: {
                                lineWidth: 5,
                                strokeColor: 'blue'
                            }
                        }
                    );
                    map.addObject(routePolyline);
                });

            }

            setTimeout(function() {
                // dataNamaKost.forEach(renderTabelKost);
                // var ord = 1;
                for (let i = 0; i < dataNamaKost.length; i++) {
                    console.log(dataJarak[i]);
                    let ord = i + 1;
                    appKost.dataKost.push({
                        nama: dataNamaKost[i],
                        no: ord,
                        jarak: dataJarak[i]
                    });

                }

            }, 3000);

            setTimeout(function() {
                $("#tblKost").dataTable({
                    "order": [
                        [1, "asc"]
                    ]
                });
            }, 3500);

        });
    }

    /** fungsi untuk menampilkan table data kost */
    var appKost = new Vue({
        el: '#divKost',
        data: {
            dataKost: [],
            namaKost: ''
        },
        methods: {
            detailAtc: function(kos, jarak) {
                axios.get("{{ env('APP_URL') }}/api/data-kost/detail/" + kos).then(function(res) {
                    // let group = new H.map.Group();
                    // map.removeObject(group);
                    console.log(jarak);
                    document.querySelector("#capNamaKost").innerHTML = res.data.nama_kos;
                    document.querySelector("#capTipeKost").innerHTML = res.data.type_kos;
                    document.querySelector("#capAlamat").innerHTML = res.data.alamat;
                    document.querySelector("#capDeks").innerHTML = res.data.deskripsi_kos;
                    kosDipilih = res.data.slug;
                    let idKost = res.data.id;
                    let strLat = res.data.latitude;
                    let strLng = res.data.longitude;
                    let wp1 = strLat + "," + strLng;
                    let wpo = latDef + "," + lngDef;
                    let gabungan = '' + wp1 + '';
                    let params = {
                        mode: "fastest;car;traffic:enabled",
                        waypoint0: wpo,
                        waypoint1: gabungan,
                        representation: "display"
                    };
                    let router = platform.getRoutingService();


                    router.calculateRoute(params, (result) => {
                        // console.log(result.response.route[0]);
                        let routeHasil = result.response.route[0];
                        // console.log(routeHasil.leg[0].maneuver);
                        let stepHasil = routeHasil.leg[0].maneuver;
                        let routeLineString = new H.geo.LineString();
                        result.response.route[0].shape.forEach(point => {
                            let [lat, lng] = point.split(",");
                            routeLineString.pushPoint({
                                lat: lat,
                                lng: lng
                            });
                        });
                        let color = ['red', 'blue', 'green', 'black', 'aqua', 'silver', 'gold', 'brown, purple', 'navy'];
                        let warnaAcak = random_item(color);
                        // console.log(warnaAcak);
                        let routePolyline = new H.map.Polyline(
                            routeLineString, {
                                style: {
                                    lineWidth: 10,
                                    strokeColor: warnaAcak,
                                    lineDash: [0, 2],
                                    lineTailCap: 'arrow-tail',
                                    lineHeadCap: 'arrow-head'
                                }
                            }
                        );
                        map.addObject(routePolyline);
                        // map.removeObject(router);
                    });
                    // $("#modalDetailKost").modal("show");
                    // https://kos.justhasnah.me/object-kos/9?from=3.6159895834286218,98.75656099929634&to=-6.36198,107.17383
                    // document.querySelector("#btnLinkDetail").setAttribute('href', "{{ env('APP_URL') }}/object-kos/" + idKost + "?from=" + latDef + "," + lngDef + "&to=" + strLat + "," + strLng);
                    // window.open("{{ env('APP_URL') }}/object-kos/" + idKost + "?from=" + latDef + "," + lngDef + "&to=" + strLat + "," + strLng + "&idKost=" + idKost + "&jarak=" + jarak)
                    window.open("{{ env('APP_URL') }}/object-kos/" + idKost + "/" + jarak)

                });

            }
        }
    });

    function random_item(items) {
        return items[Math.floor(Math.random() * items.length)];
    }

    /**fungsi untuk cari sekitar trisakti */
    function setPosition(lat = null, long = null) {
        document.getElementById('mapContainerDetail').innerHTML = '';
        console.log(lat, long)
        latDef = lat;
        lngDef = long;
        platform = new H.service.Platform({
            'apikey': apiKey
        });
        defaultLayers = platform.createDefaultLayers();
        const defaultPos = {
            lat: latDef,
            lng: lngDef
        }
        map = new H.Map(
            document.getElementById('mapContainerDetail'),
            defaultLayers.vector.normal.map, {
                zoom: 14,
                center: defaultPos,
                pixelRatio: window.devicePixelRatio || 1,
            });


        let ui = H.ui.UI.createDefault(map, defaultLayers);
        let mapEvents = new H.mapevents.MapEvents(map);
        let behavior = new H.mapevents.Behavior(mapEvents);

        var icon = new H.map.Icon(base_url + "/placeholder.png");
        var marker = new H.map.Marker(defaultPos, {
            icon: icon
        });
        map.addObject(marker);
        let dataJarak2 = [];
        let dataIdKost = [];
        let dataNamaKost = [];


        axios.get("{{ env('APP_URL') }}/api/data-kost/all").then(function(res) {
            let dataKost = res.data;
            dataKost.forEach(renderKost);

            function renderKost(item, index) {
                let namaKost = dataKost[index].nama_kos;
                // let idKost = dataKost[index].id;
                dataIdKost.push(dataKost[index].id);
                dataNamaKost.push(dataKost[index].nama_kos);
                let lat = dataKost[index].latitude;
                let lng = dataKost[index].longitude;
                let iconMarkerKost = new H.map.Icon(base_url + "/hotel.png");
                let posKost = {
                    lat: lat,
                    lng: lng
                }
                let markerKost = new H.map.Marker(posKost, {
                    icon: iconMarkerKost
                });
                map.addObject(markerKost);
                let strLat = lat.toString();
                let strLng = lng.toString();
                let wp1 = strLat + "," + strLng;
                let gabungan = '' + wp1 + '';
                let wpo = latDef + "," + lngDef;
                let asli = "-6.356927058201095,107.17425561491602";
                // route 
                let params = {
                    mode: "fastest;car;traffic:enabled",
                    waypoint0: wpo,
                    waypoint1: gabungan,
                    representation: "display"
                };

                let router = platform.getRoutingService();
                var totalJarak = 0;
                router.calculateRoute(params, (result) => {
                    // console.log(result.response.route[0]);
                    let routeHasil = result.response.route[0];
                    // console.log(routeHasil.leg[0].maneuver);
                    let stepHasil = routeHasil.leg[0].maneuver
                    stepHasil.forEach(renderStep);

                    function renderStep(item, index) {
                        // console.log(stepHasil[index].length);
                        totalJarak = totalJarak + stepHasil[index].length;
                    }
                    var jarakTemp = totalJarak / 1000;
                    dataJarak2.push(jarakTemp);
                    let routeLineString = new H.geo.LineString();
                    result.response.route[0].shape.forEach(point => {
                        let [lat, lng] = point.split(",");
                        routeLineString.pushPoint({
                            lat: lat,
                            lng: lng
                        });
                    });
                    let color = ['red', 'blue', 'black', 'aqua', 'silver', 'gold'];
                    let warnaAcak = random_item(color);
                    let routePolyline = new H.map.Polyline(
                        routeLineString, {
                            style: {
                                lineWidth: 5,
                                strokeColor: 'blue'
                            }
                        }
                    );
                    map.addObject(routePolyline);
                });

            }
            setTimeout(function() {
                appKost.dataKost = [];
                for (let i = 0; i < dataNamaKost.length; i++) {
                    // console.log(dataJarak2[i]);
                    let ord = i + 1;
                    appKost.dataKost.push({
                        nama: dataNamaKost[i],
                        no: ord,
                        jarak: dataJarak2[i]
                    });

                }

            }, 3000);
            // console.log(appKost.dataKost)

        });
    }
</script>

@endpush