  <!-- JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.27.2/axios.min.js"></script>

  <script src="{{ asset('frontend/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('frontend/lib/popper/popper.min.js') }}"></script>
  <script src="{{ asset('frontend/lib/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('frontend/lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('frontend/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/lib/scrollreveal/scrollreveal.min.js') }}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('frontend/contactform/contactform.js') }}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('frontend/js/main.js') }}"></script>

    <script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
    <script src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
    <!-- <script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script> -->
    <script src="//cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <!-- <script src="{{ asset('backend/assets/js/here.js') }}"></script> -->

    @empty($halaman)
    <script src="{{ asset('backend/assets/js/here.js') }}"></script>
    @endempty
    

  <script>
    window.hereApiKey = "{{ env('HERE_API_KEY') }}";
    var server = "{{ env('APP_URL') }}/";
  </script>