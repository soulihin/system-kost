<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- <div class="copyright-footer"> -->
        <p class="copyright color-text-a">
          &copy;
          Usakti<span class="text-success"> Kos</span> - <?php echo date('Y'); ?> <i class="fa fa-home"></i>
        </p>
      </div>
    </div>
  </div>
  </div>
</footer>