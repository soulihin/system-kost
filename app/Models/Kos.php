<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kos extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'nama_kos', 'latitude', 'longitude', 'alamat', 'type_kos', 'aturan_kos', 'deskripsi_kos', 'luas_kos', 'status', 'slug', 'is_booking', 'biaya_booking'
    ];

    public function kosgambar()
    {
        return $this->hasMany(Gambar_kos::class, 'kos_id');
    }

    public function codespacephoto()
    {
        return $this->hasMany(CodespacePhoto::class, 'kos_id', 'id');
    }

    public function getKos($latitude, $longitude)
    {
        // return $this->select('kos12.*')
        //     ->selectRaw(
        //         '( 6371 *
        //             acos( cos( radians(?) ) *
        //                 cos( radians( latitude ) ) *
        //                 cos( radians(longitude ) - radians(?)) +
        //                 sin( radians(?) ) *
        //                 sin( radians( latitude ) )
        //             )
        //         ) AS distance',
        //         [$latitude, $longitude, $jarak]
        //     )
        //     ->havingRaw("distance < ?", [$jarak])
        //     ->orderBy('distance', 'asc');
        // return $this->select('kos.*')
        //     ->selectRaw('get_distance(-6.1706799,106.7892445,kos.latitude,kos.longitude) AS distance_kos')
        //     ->selectRaw('get_distance(-6.1706799,106.7892445,?,?) AS distance_max', [$latitude, $longitude])
        //     ->havingRaw("distance_kos < distance_max")
        //     ->orderBy('distance_kos', 'asc');
        $kos = DB::table('kos')
            ->selectRaw("kos.*")
            ->selectRaw("get_distance('-6.1706799','106.7892445',kos.latitude,kos.longitude) AS distance_kos")
            ->selectRaw("get_distance('-6.1706799','106.7892445','$latitude','$longitude') AS distance_max")
            ->havingRaw('distance_kos <= distance_max',)
            ->orderBy('distance_kos', 'asc');
        return $kos;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function fasilitas()
    {
        return $this->belongsToMany('App\Models\Fasilitas');
    }

    public function testimonial()
    {
        return $this->hasMany(Testimonial::class, 'kos_id');
    }

    public function gallery()
    {
        return $this->hasMany(Gambar_kos::class, 'kos_id');
    }

    public function kamar()
    {
        return $this->hasMany(Kamar::class, 'kos_id');
    }

    public function kosTersimpan()
    {
        return $this->hasMany(Kos_tersimpan::class, 'kos_id');
    }
}
