<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kos;

class KosController extends Controller
{
    public function getKos(Request $request, $lat, $lng)
    {
        $kos = new Kos();
        return $kos->getKos($lat, $lng)->get();
    }
    public function dataKostAll()
    {
        $dataKost = Kos::with('kamar')->get();
        return \Response::json($dataKost);
        // return \Response::json(Kos::all());
    }
    public function dataDetailKost(Request $request, $namaKost)
    {
        $dataKost = Kos::where('nama_kos', $namaKost)->first();
        return \Response::json($dataKost);
    }
    public function dataDetailKostId(Request $request, $IdKost)
    {
        $dataKost = Kos::where('id', $IdKost)->get();
        return \Response::json($dataKost);
    }
}
